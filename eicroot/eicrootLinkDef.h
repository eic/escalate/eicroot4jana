
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class EicGeoMap+;
#pragma link C++ class EicDetName+;

#pragma link C++ class EicMoCaPoint+;

#pragma link C++ class EicNamePatternHub<unsigned>+;
#pragma link C++ class EicNamePatternHub<double>+;
//#pragma link C++ class EicNamePatternHub<SteppingType>+;
//#pragma link C++ class std::pair<TString,SteppingType>+;
#pragma link C++ class std::pair<TString,unsigned>+;

#pragma link C++ class EicNamePatternHub<Color_t>+;
#pragma link C++ class EicNamePatternHub<Char_t>+;
#pragma link C++ class std::pair<TString,Color_t>+;
#pragma link C++ class std::pair<TString,Char_t>+;

#pragma link C++ class EicBitMask<UGeantIndex_t>+;
#pragma link C++ class EicBitMask<ULogicalIndex_t>+;
// This should in principle work as well?;
//#pragma link C++ class EicBitMask*+;
#pragma link C++ class GeantVolumeLevel+;
#pragma link C++ class SourceFile+;
#pragma link C++ class LogicalVolumeGroupProjection+;
#pragma link C++ class LogicalVolumeGroup+;
#pragma link C++ class EicGeoParData+;
#pragma link C++ class LogicalVolumeLookupTableEntry+;
#pragma link C++ class std::pair<ULong64_t,ULong64_t>+;
#pragma link C++ class std::pair<TString, Int_t>+;

#pragma link C++ namespace eic;

#endif
