
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class Poacher+;
//#pragma link C++ class EventEicMC+;
#pragma link C++ class EicProtoGenerator+;
#pragma link C++ class EicBoxGenerator+;
#pragma link C++ class EicEventGenerator+;
#pragma link C++ class ParticleMappingTable+;

//#pragma link C++ class EicEventGeneratorTask+;
//#pragma link C++ class EicSmearTask+;

#endif
